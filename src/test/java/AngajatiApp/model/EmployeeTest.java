package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    private Employee e;


    @BeforeEach
    void setUp() throws Exception {
        e = new Employee();
        //e.setIdAngajat(Integer.parseInt("1"));
        e.setFirstName("");
        e.setLastName("Surugiu");
        e.setFunction(DidacticFunction.valueOf("ASISTENT"));
        e.setCnp("123");
        e.setSalary(Double.valueOf("9000"));
        System.out.println("Setup employee");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    @Order(8)
    void getFirstName() {
        assertEquals("", e.getFirstName(), "Numele nu este Iulia");
        System.out.println("Numele este ok");
    }

    @Test
    @Order(2)
    void setFirstName() {
        e.setFirstName("Dana");
        assertEquals("Dana", e.getFirstName(), "Numele nu este Dana");
        System.out.println("Numele este ok");
    }

    @Test
    @Order(5)
    void getFunction() {
        assertEquals(DidacticFunction.valueOf("ASISTENT"), e.getFunction(), "Functia nu este ASISTENT");
        System.out.println("Functia este ok");
    }

    @Test
    @Order(6)
    void setFunction() {
        e.setFunction(DidacticFunction.valueOf("LECTURER"));
        assertEquals(DidacticFunction.valueOf("LECTURER"), e.getFunction(), "Functia nu este LECTURER");
        System.out.println("Functia este ok");
    }

    @Test
    @Order(3)
    void getSalary() {
        assertEquals(Double.valueOf("9000"), e.getSalary(), "Salariul nu este 9000");
        System.out.println("Salariul este ok");
    }

    @Test
    @Order(4)
    void setSalary() {
        e.setSalary(Double.valueOf("9900"));
        assertEquals(Double.valueOf("9900"), e.getSalary(), "Salariul nu este 9900");
        System.out.println("Salariul este ok");

    }

    @Test
    @Order(1)
    void testConstructor() {
        Employee e1 = new Employee();
        assertEquals("", e1.getFirstName(), "Numele nu ar trebui sa fie completat");
        System.out.println("Constructorul este ok");
    }

    @ParameterizedTest
    @ValueSource(strings = {"iuliana", "ionut", "marin", "andrei"})
    void testParametrizatSetareNume(String nume) {
        e.setFirstName(nume);
        assertEquals(nume, e.getFirstName(), "numele nu este" + nume);
        System.out.println("setare Nume " + nume + " este ok");
    }


    @Disabled
    @Test
    @Timeout(1)
    void deneoprit() {
        int i = 0;
        while (i == 0) {
            System.out.println("0101000010101111101");
        }
    }

    @Test
    void timeoutexceed() {
        assertTimeout(Duration.ofMillis(200), () -> {
            Thread.sleep(100);
        });
    }

    @Test
    void test_setSalary_Valid() {
        try {
            e.setSalary((double) 1100);
            assert (true);
        } catch (Exception e) {
            assert (false);
        }
        System.out.println("Salariul este valid");
    }
}