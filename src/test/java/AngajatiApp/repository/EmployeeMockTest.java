package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static AngajatiApp.controller.DidacticFunction.*;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private Employee e;
    EmployeeController employee_ctrl;
    int numberOfEmployees;

    @BeforeEach
    void setUp() {
        EmployeeMock employeeRepository = new EmployeeMock();
        employee_ctrl = new EmployeeController(employeeRepository);
        try {
            numberOfEmployees = employee_ctrl.getEmployeesList().size();
        } catch (Exception exception) {

        }
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void testAddEmployeeTC1() { //(date invalide)
        e = new Employee();
        e.setLastName("pa");
        e.setFirstName("ionel");
        e.setCnp("1234567890876");
        e.setFunction(ASISTENT);
        e.setSalary(2500.0);

        employee_ctrl.addEmployee(e);//(date invalide
        assertEquals(numberOfEmployees, employee_ctrl.getEmployeesList().size(), "nu s-a facut adaugarea angajatului");
    }

    @Test
    void testAddEmployeeTC2() { //(date valide)
        e = new Employee();
        e.setLastName("ione");
        e.setFirstName("ionela");
        e.setCnp("1234567890876");
        e.setFunction(LECTURER);
        e.setSalary(2500.0);

        employee_ctrl.addEmployee(e);//(date valide)
        assertEquals(numberOfEmployees + 1, employee_ctrl.getEmployeesList().size());

    }

    @Test
    void testAddEmployeeTC4() { //(date invalide)
        e = new Employee();
        e.setLastName("ionica");
        e.setFirstName("costi");
        e.setCnp("1234567890876");
        e.setFunction(CONFERENTIAR);
        e.setSalary(0.0);

        employee_ctrl.addEmployee(e);//(date invalide)
        assertEquals(numberOfEmployees, employee_ctrl.getEmployeesList().size(), "nu s-a aruncat exceptie");

    }

    @Test
    void testAddEmployeeTC6() { //(date invalide)
        e = new Employee();
        e.setLastName("io");
        e.setFirstName("Vasile");
        e.setCnp("1234567890876");
        e.setFunction(TEACHER);
        e.setSalary(6001.0);


        employee_ctrl.addEmployee(e);//(date invalide)
        assertEquals(numberOfEmployees, employee_ctrl.getEmployeesList().size(), "nu s-a aruncat exceptie");
    }

    @Test
    void testAddEmployeeTC9() {//(date valide)
        e = new Employee();
        e.setLastName("Dum");
        e.setFirstName("Mihai");
        e.setCnp("1234567890876");
        e.setFunction(LECTURER);
        e.setSalary(2500.0);

        employee_ctrl.addEmployee(e);//(date valide)
        assertEquals(numberOfEmployees + 1, employee_ctrl.getEmployeesList().size(), "nu s-a aruncat exceptie");
    }

    //lab4

    Employee Vasile = new Employee("Dorel", "Georgescu", "1234567890876", TEACHER, 2500d);

    @Test
    void testModifyEmployeeFunctionTC1() {
        EmployeeMock repositoryMockEmployee = new EmployeeMock();
        repositoryMockEmployee.modifyEmployeeFunction(Vasile, ASISTENT);
        assertEquals(ASISTENT, repositoryMockEmployee.getEmployeeList().get(5).getFunction());
    }

    @Test
    void testModifyEmployeeFunctionTC2() {
        EmployeeMock repositoryMockEmployee = new EmployeeMock();
        Employee employee = null;

        repositoryMockEmployee.modifyEmployeeFunction(employee, DidacticFunction.CONFERENTIAR);
        boolean ExistaAngajat = false;
        int i = 0;
        while (i < repositoryMockEmployee.getEmployeeList().size()) {
            Employee employeeN = repositoryMockEmployee.getEmployeeList().get(i);
            if (employeeN.getFunction() == DidacticFunction.CONFERENTIAR)
                ExistaAngajat = true;
            i++;
        }
        assertEquals(false, ExistaAngajat, "nu ar trebui sa ajunga aici");
    }

}
